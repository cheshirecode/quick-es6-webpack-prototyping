# Basic Webpack + ES6 starter

Based on https://github.com/wbkd/yet-another-webpack-es6-starterkit.

Tweaked to fit in with my prototyping needs:

- Use eslint with 'google' style.
- Reverse proxy on webpack dev server.
- ES6 with experimental Babel for new specs goodness.

### Installation

`npm i` OR `yarn`
### Dev server

```
npm run dev
```

### Build

```
npm run build
```