'use strict';

const Path = require('path');
const Webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractSASS = new ExtractTextPlugin('styles/bundle.css');
const path = require('path');

module.exports = (options) => {
  let webpackConfig = {
    devtool: options.devtool,
    entry: [
      `webpack-dev-server/client?http://localhost:${options.port}`,
      'webpack/hot/dev-server',
      './src/js/index',
    ],
    output: {
      path: Path.join(__dirname, 'build'),
      filename: 'bundle.js',
    },
    plugins: [
      new Webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development'),
        },
      }),
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
    ],
    resolve: {
      root: path.join(__dirname, './src'),
    },
    resolveLoader: {
      root: path.join(__dirname, './node_modules'),
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',
        },
        {
          test: /\.(woff2?|ttf|eot|svg|png|ico)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file?name=fonts/[name].[ext]',
        },
        // { test: /\.scss$/, loaders: ['style', 'css', 'postcss', 'sass'] },
        // {
        //   test: /\.(woff2?|ttf|eot|svg)$/,
        //   loader: 'url?limit=10000',
        // },
      ],
    },
  };

  if (options.isProduction) {
    webpackConfig.entry = ['./src/js/index'];

    webpackConfig.plugins.push(
      new Webpack.optimize.OccurenceOrderPlugin(),
      new Webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false,
        },
      }),
      ExtractSASS
    );

    webpackConfig.module.loaders.push({
      test: /\.(scss|css)$/i,
      loader: ExtractSASS.extract(['css', 'sass']),
    });
  } else {
    webpackConfig.plugins.push(
      new Webpack.HotModuleReplacementPlugin()
    );

    webpackConfig.module.loaders.push({
      test: /\.(scss|css)$/i,
      loaders: ['style', 'css', 'sass'],
    }, {
      test: /\.js$/,
      loader: 'eslint',
      exclude: /node_modules/,
    });

    webpackConfig.devServer = {
      contentBase: './build',
      hot: true,
      port: options.port,
      inline: true,
      progress: true,
      proxy: {
        '/api': {
          target: {
            'host': 'localhost',
            'protocol': 'http:',
            'port': 8765,
          },
          pathRewrite: {
            '^/api': '',
          },
          ignorePath: false,
          changeOrigin: true,
          secure: false,
        },
      },
    };
  }
  return webpackConfig;
};
